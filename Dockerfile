FROM golang:alpine AS build

WORKDIR /ciklum

COPY . .

RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/ciklum


FROM scratch

WORKDIR /go/bin

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /go/bin/ciklum ./ciklum
COPY --from=build /ciklum/config/config.yaml ./config/config.yaml

ENTRYPOINT ["/go/bin/ciklum"]

EXPOSE 8080
