# Interview Task API

## Run application

Run binary:
```
go run .
```

Open in browser [http://localhost:8080/](http://localhost:8080/)

Port can be changed in configuration file ```config/config.yaml```

Connection read, write timeout is 5 seconds.


## Run application in Docker

Build image:
```
docker build -t ciklum-app .
```

Run container:
```
docker run -p 8080:8080 --name ciklum ciklum-app
```

Using custom port:
```
docker run -p {PORT}:8080 --name ciklum ciklum-app
```

Open in browser [http://localhost:8080/](http://localhost:8080/)
