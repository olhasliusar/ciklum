package api

import (
	"ciklum/delegate"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

type HttpIndex struct {
	router gin.IRouter
}

func (a *HttpIndex) Init() {
	a.router.GET(IndexRoute, a.handler)
}

func (a *HttpIndex) handler(c *gin.Context) {

	data, err := delegate.GenerateResponse()

	if err != nil {
		log.Println(err)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": data,
	})
}

func NewIndex(router gin.IRouter) *HttpIndex {
	res := new(HttpIndex)
	res.router = router

	return res
}
