package api

type IHttpAction interface {
	// Init initializes action
	Init()
}
