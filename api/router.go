package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

// HTTP routes
const IndexRoute = "/"

func Run(httpPort int) error {

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()

	s := &http.Server{
		Addr:         fmt.Sprintf(":%d", httpPort),
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	addActions(router)

	log.Printf("Http server is listening port %d", httpPort)
	if err := s.ListenAndServe(); err != nil {
		return fmt.Errorf("failed on running web server: %s", err)
	}
	return nil
}

func addActions(router *gin.Engine) {

	actions := []IHttpAction{
		NewIndex(router),
	}

	// init actions
	for _, a := range actions {
		a.Init()
	}
}
