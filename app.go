package main

import (
	"ciklum/api"
	"ciklum/client"
	"ciklum/config"
)

type Application struct {
	Name string

	conf *config.Config
}

func (a *Application) AbsorbConfig(conf *config.Config) {
	a.conf = conf
}

func (a *Application) InitDependency() {
	client.InitClient(
		a.conf.Source.Article.Url,
		a.conf.Source.ContentMarketing.Url,
	)
}

func (a Application) Run() error {
	return api.Run(a.conf.Http.Port)
}

func NewApplication(name string) *Application {
	app := new(Application)

	app.Name = name

	return app
}
