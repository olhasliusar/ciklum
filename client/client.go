package client

var cl client

type client struct {
	articleUrl          string
	contentMarketingUrl string
}

func InitClient(articleUrl, contentMarketingUrl string) {
	cl = client{
		articleUrl:          articleUrl,
		contentMarketingUrl: contentMarketingUrl,
	}
}
