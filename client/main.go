package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Response struct {
	HttpStatus int `json:"httpStatus"`
	Resp       struct {
		Items json.RawMessage `json:"items"`
	} `json:"response"`
}

func fetchData(url string) ([]byte, error) {
	var res Response

	response, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("can't send request to %s, err: %s", url, err)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("can't read response from %s, err: %s", url, err)
	}

	if err := json.Unmarshal(body, &res); err != nil {
		return nil, fmt.Errorf("incorrect response format, err: %s", err)
	}

	if res.HttpStatus < 200 || res.HttpStatus > 299 {
		return nil, fmt.Errorf("response status is %d", res.HttpStatus)
	}

	return res.Resp.Items, nil
}

func GetArticles() ([]*Article, error) {
	var res []*Article

	if bytes, err := fetchData(cl.articleUrl); err != nil {
		return nil, fmt.Errorf("can`t make request, err: %s", err)
	} else if err := json.Unmarshal(bytes, &res); err != nil {
		return nil, fmt.Errorf("incorrect items response format, err: %s", err)
	}

	return res, nil
}

func GetContentMarketing() ([]*ContentMarketing, error) {
	var res []*ContentMarketing

	if bytes, err := fetchData(cl.contentMarketingUrl); err != nil {
		return nil, fmt.Errorf("can`t make request, err: %s", err)
	} else if err := json.Unmarshal(bytes, &res); err != nil {
		return nil, fmt.Errorf("incorrect items response format, err: %s", err)
	}

	return res, nil
}
