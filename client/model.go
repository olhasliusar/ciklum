package client

const addType = "Ad"

type Article struct {
	Type         string  `json:"type,omitempty"`
	HarvesterId  string  `json:"harvesterId,omitempty"`
	CerebroScore float64 `json:"cerebro-score,omitempty"`
	Url          string  `json:"url,omitempty"`
	Title        string  `json:"title,omitempty"`
	CleanImage   string  `json:"cleanImage,omitempty"`
}

type ContentMarketing struct {
	Article           `json:",inline"`
	CommercialPartner string `json:"commercialPartner,omitempty"`
	LogoURL           string `json:"logoURL,omitempty"`
}

func NewAdd() *ContentMarketing {
	return &ContentMarketing{
		Article: Article{
			Type: addType,
		},
	}
}
