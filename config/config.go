package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

const configFile = "config/config.yaml"

type Config struct {
	Http   Http   `yaml:"http"`
	Source Source `yaml:"source"`
}

type Http struct {
	Port int `yaml:"port"`
}

type Source struct {
	Article          Api `yaml:"article"`
	ContentMarketing Api `yaml:"contentMarketing"`
}

type Api struct {
	Url string `yaml:"url"`
}

func NewConfig() (*Config, error) {

	conf := new(Config)

	data, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, fmt.Errorf("can't read config file %s: %s", configFile, err)
	}

	err = yaml.Unmarshal(data, &conf)
	if err != nil {
		return nil, fmt.Errorf("can't parse config from file %s: %s", configFile, err)
	}

	return conf, nil
}
