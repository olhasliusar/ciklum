package delegate

import (
	"ciklum/client"
	"log"
	"sync"
)

func GenerateResponse() ([]*client.ContentMarketing, error) {

	var (
		wg  sync.WaitGroup
		err error

		articles         []*client.Article
		contentMarketing []*client.ContentMarketing
	)

	chErr := make(chan error, 2)

	wg.Add(2)

	go func(chErr chan<- error) {
		defer wg.Done()
		articles, err = client.GetArticles()

		if err != nil {
			log.Println(err)
			chErr <- err
		}
	}(chErr)

	go func(chErr chan<- error) {
		defer wg.Done()
		contentMarketing, err = client.GetContentMarketing()

		if err != nil {
			log.Println(err)
			chErr <- err
		}
	}(chErr)

	wg.Wait()

	select {
	case err := <-chErr:
		return nil, err
	default:
	}

	return mergeArr(articles, contentMarketing), nil
}

func mergeArr(a []*client.Article, cm []*client.ContentMarketing) []*client.ContentMarketing {

	var (
		result []*client.ContentMarketing
		end    int
	)

	for i, j := 0, 0; i < len(a); i, j = i+5, j+1 {

		if i+5 < len(a) {
			end = i + 5
		} else {
			end = len(a)
		}

		articles := a[i:end]
		for _, article := range articles {
			result = append(result, &client.ContentMarketing{Article: *article})
		}

		if j < len(cm) {
			result = append(result, cm[j])
		} else {
			result = append(result, client.NewAdd())
		}
	}

	return result
}
