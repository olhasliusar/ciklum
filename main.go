package main

import (
	"ciklum/config"
	"log"
)

const name = "api-service"

var application *Application

func init() {
	var (
		conf *config.Config
		err  error
	)

	application = NewApplication(name)

	if conf, err = config.NewConfig(); err != nil {
		log.Fatalf("can`t fetch configuration, err: %s", err)
	}

	application.AbsorbConfig(conf)
	application.InitDependency()
}

func main() {

	if err := application.Run(); err != nil {
		log.Fatalf("application failed, err: %s", err)
	}
}
